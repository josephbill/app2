package com.emobilis.appconcepts2.model;

public class RecyclerModel {
    //declare the variables
    private String productName;
    private String productDesc;
    private String productPrice;
    private String phoneContact;
    private String emailContact;
    private int imageResource;

    //constructor
    public RecyclerModel(String productName,String productDesc, String productPrice, String phoneContact, String emailContact, int imageResource){
        this.productName = productName;
        this.productDesc = productDesc;
        this.productPrice = productPrice;
        this.phoneContact = phoneContact;
        this.emailContact = emailContact;
        this.imageResource = imageResource;
    }

    //get and set


    public String getEmailContact() {
        return emailContact;
    }

    public void setEmailContact(String emailContact) {
        this.emailContact = emailContact;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getPhoneContact() {
        return phoneContact;
    }

    public void setPhoneContact(String phoneContact) {
        this.phoneContact = phoneContact;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }
}
