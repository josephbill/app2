package com.emobilis.appconcepts2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.emobilis.appconcepts2.adapters.RecyclerAdapter;
import com.emobilis.appconcepts2.model.RecyclerModel;

import java.util.ArrayList;

public class RecyclerViewActivity extends AppCompatActivity {
    //declare the recyclerview
    RecyclerView recyclerView;
    //declare the adapter
    RecyclerAdapter recyclerAdapter;
    //layoutmanager
    private  RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        //set on create list
        ArrayList<RecyclerModel> recyclerModels = new ArrayList<>();
        //add info to my model using the arraylist ref
        recyclerModels.add(new RecyclerModel("Nokia","X","Ksh. 10,000","0798501657","joseph.bill@emobilis.org",R.drawable.ic_smartphone_black_24dp));
        recyclerModels.add(new RecyclerModel("Samsung","A5","Ksh. 30,000","0798501657","joseph.bill@emobilis.org",R.drawable.ic_phone_android_black_24dp));
        recyclerModels.add(new RecyclerModel("iPhone","7","Ksh. 100,000","0798501657","joseph.bill@emobilis.org",R.drawable.ic_phonelink_setup_black_24dp));

        //ref to my recyclerview widget
        recyclerView = findViewById(R.id.recyclerProducts);
        recyclerView.setHasFixedSize(true); //use size of arraylist items
        layoutManager = new LinearLayoutManager(this); //intializing the layout manager
        //create a new instance of the adapter class
        recyclerAdapter = new RecyclerAdapter(this,recyclerModels);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerAdapter);
    }
}
