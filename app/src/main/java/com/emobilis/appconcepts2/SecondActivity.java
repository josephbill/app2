package com.emobilis.appconcepts2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    TextView tvset;
    private static final String TAG = "SecondActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        tvset = findViewById(R.id.textView);

        Intent intent = getIntent();
        String text = intent.getStringExtra("text");
        Log.d(TAG,"shared text is " + text);

        tvset.setText(text);
    }
}
