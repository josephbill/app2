package com.emobilis.appconcepts2;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CustomAdapter extends ArrayAdapter<String> {
    //instance variable to hold data from activity class
    private final Activity context;
    private final String[] mobile_phones;
    private final String[] prices;
    private final Integer[] images;

     //constructor , data from activity class
    public CustomAdapter(Activity context,String[] mobile_phones,String[] prices,Integer[] images) {
        super(context,R.layout.custom_list,mobile_phones);

        this.context = context;
        this.mobile_phones = mobile_phones;
        this.prices = prices;
        this.images = images;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView =inflater.inflate(R.layout.custom_list, null,true);

        //references belonging to the layout group
        TextView name_of_phone = rowView.findViewById(R.id.name_phone);
        TextView price = rowView.findViewById(R.id.price);
        ImageView image_phone = rowView.findViewById(R.id.icon);

        //i can set the data to my view
        name_of_phone.setText(mobile_phones[position]);
        price.setText(prices[position]);
        image_phone.setImageResource(images[position]);

        return rowView;
}
}
