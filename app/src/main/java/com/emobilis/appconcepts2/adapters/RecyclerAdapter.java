package com.emobilis.appconcepts2.adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.emobilis.appconcepts2.DetailsActivity;
import com.emobilis.appconcepts2.R;
import com.emobilis.appconcepts2.model.RecyclerModel;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {
    //arraylist to store my info in a list
    private static final int REQUEST_CALL = 1;
    private ArrayList<RecyclerModel> recyclerModels;
    Context context;

    //constructor
    public RecyclerAdapter(Context context,ArrayList<RecyclerModel> recyclerModelArrayList){
        this.context = context;
        recyclerModels = recyclerModelArrayList;
    }

    //create my viewholder
    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{
         //declare views
         TextView name,desc;
         ImageView image,phone;
         Button btnDetails;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            //find the ref
            name = itemView.findViewById(R.id.productName);
            desc = itemView.findViewById(R.id.productDesc);
            image = itemView.findViewById(R.id.imageProduct);
            phone = itemView.findViewById(R.id.callButton);
            btnDetails = itemView.findViewById(R.id.action_button_2);


        }
    }

    @NonNull
    @Override
    //here u inflate the recycled item layout
    public RecyclerAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
        RecyclerAdapter.RecyclerViewHolder recyclerViewHolder = new RecyclerAdapter.RecyclerViewHolder(v); //new instance
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.RecyclerViewHolder holder, final int position) {
           //create a reference to the items in the arraylist and store inside that reference the position of the item in the list
          final RecyclerModel recyclerModel = recyclerModels.get(position);

          //set views to recycled items
          holder.name.setText(recyclerModel.getProductName());
          holder.desc.setText(recyclerModel.getProductDesc());
          holder.image.setImageResource(recyclerModel.getImageResource());

          //making a call
         holder.phone.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 //calling method to execute call
                 //first check if the user has allowed app to make a phone call
                 if (ContextCompat.checkSelfPermission(context,
                         Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
                     //i need to alert the user to allow the app to make a call
                     ActivityCompat.requestPermissions((Activity) context,
                             new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
                 } else {
                     //if the user has allowed app to make a call , launch intent here
                     String phone = "tel:" + recyclerModel.getPhoneContact();
                     context.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(phone)));
                 }
             }
         });

         //going to details screens with info
        holder.btnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailsActivity.class);
                //shared data
                intent.putExtra("product_name",recyclerModels.get(position).getProductName());
                intent.putExtra("product_desc",recyclerModels.get(position).getProductDesc());
                intent.putExtra("product_price",recyclerModels.get(position).getProductPrice());
                intent.putExtra("product_phone",recyclerModels.get(position).getPhoneContact());
                intent.putExtra("product_email",recyclerModels.get(position).getEmailContact());
                intent.putExtra("product_image",recyclerModels.get(position).getImageResource());
                context.startActivity(intent);
            }
        });
    }



    @Override
    public int getItemCount() {
        return recyclerModels.size();
    }
}
