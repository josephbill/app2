package com.emobilis.appconcepts2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DetailsActivity extends AppCompatActivity {

    TextView name,desc,phone,email,price;
    ImageView imageView;

    //variables
    String product_name,product_desc,product_price,product_email,product_phone;
    int image;

    //Linear layout
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        //find views
        name = findViewById(R.id.textView3);
        desc = findViewById(R.id.textView4);
        price = findViewById(R.id.textView5);
        phone = findViewById(R.id.textView6);
        email = findViewById(R.id.textView7);
        imageView = findViewById(R.id.imageView2);
        linearLayout = findViewById(R.id.lineEmail);



        //pick shared data
        Intent intent = getIntent();
        product_name = intent.getStringExtra("product_name");
        product_desc = intent.getStringExtra("product_desc");
        product_email = intent.getStringExtra("product_email");
        product_price = intent.getStringExtra("product_price");
        product_phone = intent.getStringExtra("product_phone");

        //set text
        name.setText(product_name);
        desc.setText(product_desc);
        email.setText(product_email);
        phone.setText(product_phone);
        price.setText(product_price);

        //fetch the image
        //u get multimedia files shared using the bundle getExtras method
        Bundle bundle = getIntent().getExtras();
        //check if bundle has image
        if (bundle != null){
            image = bundle.getInt("product_image");
            //set image to the imageview
            imageView.setImageResource(image);
        } else {
            Toast.makeText(this, "No shared image ", Toast.LENGTH_SHORT).show();
        }

        //email intent
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailIntent();
            }
        });

    }

    private void emailIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, product_email);
        intent.putExtra(Intent.EXTRA_SUBJECT, product_name);
        intent.putExtra(Intent.EXTRA_TEXT, "I want buy the " + product_name + "with the following description " + product_desc);
        //filtering the 3rd party applications in my android device
        intent.setType("message/rfc822");
        //create chooser to give my user app options
        startActivity(Intent.createChooser(intent, "Choose your preffered email app"));
    }
}
