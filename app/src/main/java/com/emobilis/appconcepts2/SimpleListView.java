package com.emobilis.appconcepts2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class SimpleListView extends AppCompatActivity {
    //give my items in array format
    String[] mobile_phones = {"Samsung","Nokia","Motorola","Techno","iPhone","Xiamin","Huawei"};
    ArrayAdapter adapter;
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_list_view);

        //call the ArrayAdapter class and create a new Instance of it
         adapter = new ArrayAdapter<String>(SimpleListView.this,R.layout.mobile_name,mobile_phones);
         listView = findViewById(R.id.simpleList);
         //set the adapter to listview
        listView.setAdapter(adapter);

        //onItemClicked
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //switch click based on position
                if (position == 0){
                    Toast.makeText(SimpleListView.this, "Samsung", Toast.LENGTH_SHORT).show();
                } else if(position == 1){
                    Toast.makeText(SimpleListView.this, "Nokia", Toast.LENGTH_SHORT).show();

                } else if (position == 2){
                    Toast.makeText(SimpleListView.this, "Motorola", Toast.LENGTH_SHORT).show();
                }
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    Intent intent = new Intent(SimpleListView.this,RecyclerViewActivity.class);
                    startActivity(intent);
                } else if (position == 1){
                    Toast.makeText(SimpleListView.this, "Nokia long pressed", Toast.LENGTH_SHORT).show();
                } else if (position == 2 ){
                    Toast.makeText(SimpleListView.this, "Motorola long pressed", Toast.LENGTH_SHORT).show();

                }

                return false;
            }
        });

    }

    //take me to custom list activity
    public void customList(View v){
        Intent intent = new Intent(SimpleListView.this,CustomListView.class);
        startActivity(intent);
    }
}
