package com.emobilis.appconcepts2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG,"on create is running ");

        submit = findViewById(R.id.button);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "button clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                intent.putExtra("text","Hello World");
                startActivity(intent);
            }
        });

    }

    //onStart

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "Welcome to your dashboard", Toast.LENGTH_LONG).show();
        Log.d(TAG,"on start is running ");

    }

    //on Resume

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "Resume message", Toast.LENGTH_LONG).show();
        Log.d(TAG,"on resume is running ");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Destroyed App", Toast.LENGTH_SHORT).show();
        Log.d(TAG,"on Destory is running");
    }
}
