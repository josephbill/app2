package com.emobilis.appconcepts2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

public class CustomListView extends AppCompatActivity {
    //declare views
    ListView listView;
    //array sources
    String[] mobile_phones = {"Samsung","Nokia","Motorola","Techno","iPhone","Xiamin","Huawei"};
    String[] prices ={"Ksh. 1000","Ksh. 2000","Ksh. 3000","Ksh. 4000","Ksh. 5000","Ksh. 6000","Ksh. 7000"};
    Integer[] image = {
            R.drawable.ic_smartphone_black_24dp,
            R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_foreground,
            R.drawable.ic_smartphone_black_24dp,
            R.drawable.ic_smartphone_black_24dp,
            R.drawable.ic_smartphone_black_24dp,
            R.drawable.ic_smartphone_black_24dp,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_list_view);

        //calling instance of the custom Adapter
        CustomAdapter adapter = new CustomAdapter(this,mobile_phones,prices,image);
        listView = findViewById(R.id.customList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    Intent intent = new Intent(CustomListView.this,SimpleListView.class);
                    startActivity(intent);
                }
            }
        });

    }
}
